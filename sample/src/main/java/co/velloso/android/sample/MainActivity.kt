package co.velloso.android.sample

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import co.velloso.android.inputcardview.InputCardView
import co.velloso.android.inputcardview.InputCardView.Item.Companion.TYPE_BUTTON
import co.velloso.android.inputcardview.InputCardView.Item.Companion.TYPE_INPUT
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val inputMethodManager by lazy {
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inputCardView.bankCardView = bankCardView
        var cardHolderDocument: String? = null
        val inputs = listOf(
                inputCardView.INPUT_CARD_NUMBER,
                inputCardView.INPUT_CARD_HOLDER_NAME,
                inputCardView.INPUT_CARD_EXPIRY,
                inputCardView.INPUT_CARD_SECURITY_CODE,
                InputCardView.InputItem(
                        TYPE_INPUT,
                        R.string.input_cpf_hint,
                        "",
                        InputType.TYPE_CLASS_NUMBER,
                        listOf(CpfInputFilter()),
                        null
                ) { e ->
                    e?.formatCpf()
                    cardHolderDocument = e?.toString() ?: ""
                    if (e?.length == 14) inputCardView.selectedPosition += 1
                },
                InputCardView.InputItem(
                        TYPE_BUTTON,
                        co.velloso.android.bankcardview.R.string.pay,
                        "",
                        InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS,
                        listOf(),
                        View.OnClickListener {
                            Toast.makeText(this, cardHolderDocument, Toast.LENGTH_LONG).show()
                        },
                        null
                )
        )
        inputCardView.submitList(inputs)
        inputCardView.setOnSnapListener {
            when (it) {
                3 -> bankCardView.showBack()
                inputs.size - 1 -> {
                    bankCardView.showFront()
                    inputMethodManager.hideSoftInputFromWindow(inputCardView.windowToken, 0)
                }
                else -> bankCardView.showFront()
            }
        }
    }


    fun Editable.formatCpf(): Editable {
        if (this.toString().countOccurrences(".") < 1 && this.length > 3) {
            this.insert(3, ".")
        }
        if (this.toString().countOccurrences(".") < 2 && this.length > 7) {
            this.insert(7, ".")
        }
        if (!this.toString().contains("-") && this.length > 11) {
            this.insert(11, "-")
        }
        return this
    }

    fun String.countOccurrences(occurrence: String): Int =
            this.length - this.replace(occurrence, "").length
}
