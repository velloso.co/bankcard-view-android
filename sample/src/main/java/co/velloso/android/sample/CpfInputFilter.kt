package co.velloso.android.sample

import android.text.InputFilter
import android.text.Spanned
import co.velloso.android.inputcardview.isNumeric

class CpfInputFilter : InputFilter {

    private val mMax = 14

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {

        var keep = mMax - (dest.length - (dend - dstart))
        if (keep <= 0) {
            return ""
        } else if (source.toString() == "." && dstart != 3 && dstart != 7) {
            return ""
        } else if (source.toString() == "-" && dstart != 11) {
            return ""
        } else if (!source.toString().isNumeric() && source.toString() != "-"
                && source.toString() != ".") {
            return ""
        } else if (keep >= end - start) {
            return null // keep original
        } else {
            keep += start
            if (Character.isHighSurrogate(source[keep - 1])) {
                --keep
                if (keep == start) {
                    return ""
                }
            }
            return source.subSequence(start, keep)
        }
    }
}
