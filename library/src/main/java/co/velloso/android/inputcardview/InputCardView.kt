package co.velloso.android.inputcardview

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.AbsSavedState
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import androidx.annotation.StringRes
import androidx.recyclerview.widget.*
import co.velloso.android.bankcardview.BankCardView
import co.velloso.android.bankcardview.R
import co.velloso.android.inputcardview.InputCardView.Item.Companion.TYPE_BUTTON
import co.velloso.android.inputcardview.InputCardView.Item.Companion.TYPE_CUSTOM
import co.velloso.android.inputcardview.InputCardView.Item.Companion.TYPE_INPUT
import kotlinx.android.synthetic.main.card_input_button.view.*
import kotlinx.android.synthetic.main.card_input_item.view.*
import kotlinx.android.synthetic.main.card_input_pager.view.*


class InputCardView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0

) : FrameLayout(context, attrs, defStyleAttr) {

    private val pagerSnapHelper = PagerSnapHelper()

    private var positionChangeListener: ((position: Int) -> Unit)? = null

    private var focusListener: ((v: View, hasFocus: Boolean) -> Unit)? = null

    var selectedPosition = 0
        set(value) {
            field = value
            clearFocus()
            pagerRecyclerView.smoothScrollToPosition(selectedPosition)
        }

    var bankCardView: BankCardView? = null

    val INPUT_CARD_NUMBER = InputItem(
            TYPE_INPUT,
            R.string.input_card_number_hint,
            "",
            InputType.TYPE_CLASS_NUMBER,
            listOf(BankCardNumberInputFilter()),
            null
    ) { e ->
        e?.formatCreditCard()
        bankCardView?.cardNumber = e?.toString()?.replace(" ", "")
        if (e?.length == 19) this.selectedPosition += 1
    }

    val INPUT_CARD_HOLDER_NAME = InputItem(
            TYPE_INPUT,
            R.string.input_card_holder_name_hing,
            "",
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS,
            listOf(),
            null
    ) { e ->
        bankCardView?.cardHolderName = e?.toString()?.toUpperCase() ?: ""
    }

    val INPUT_CARD_EXPIRY = InputItem(
            TYPE_INPUT,
            R.string.input_card_expiry_hint,
            "",
            InputType.TYPE_CLASS_DATETIME or InputType.TYPE_DATETIME_VARIATION_DATE,
            listOf(ValidThroughInputFilter()),
            null
    ) { e ->
        e?.formatValidThrough()
        bankCardView?.expiry = e?.toString() ?: ""
        if (e?.length == 5) this.selectedPosition += 1
    }

    val INPUT_CARD_SECURITY_CODE = InputItem(
            TYPE_INPUT,
            R.string.input_card_security_code_hint,
            "",
            InputType.TYPE_CLASS_NUMBER,
            listOf(),
            null
    ) { e ->
        bankCardView?.securityCode = e?.toString() ?: ""
        if (e?.length == bankCardView?.getCardType()?.securityCodeLength) this.selectedPosition += 1
    }

    init {
        inflate(context, R.layout.card_input_pager, this)

        // Initialize date and buttons
        nextButton.setOnClickListener {
            selectedPosition += 1
        }
        previousButton.setOnClickListener {
            if (selectedPosition > 0) {
                selectedPosition -= 1
            }
        }

        pagerRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false).apply {
            initialPrefetchItemCount = 10
        }
        pagerRecyclerView.recycledViewPool.setMaxRecycledViews(TYPE_CUSTOM, 0)
        pagerRecyclerView.recycledViewPool.setMaxRecycledViews(TYPE_INPUT, 0)
        val pagerAdapter = PagerAdapter()
        pagerRecyclerView.adapter = pagerAdapter
        pagerSnapHelper.attachToRecyclerView(pagerRecyclerView)
        pagerRecyclerView.addOnScrollListener(
                SnapOnScrollListener(pagerSnapHelper, SnapOnScrollListener.NOTIFY_ON_SCROLL_STATE_IDLE) { v, position ->

                    selectedPosition = position

                    v.inputEditText?.requestFocus()
                    v.inputButton?.requestFocus()

                    previousButton.visibility = if (selectedPosition == 0) GONE
                    else VISIBLE
                    nextButton.visibility = if (selectedPosition == pagerAdapter.itemCount - 1) GONE
                    else VISIBLE

                    positionChangeListener?.invoke(position)
                }
        )
    }

    fun submitList(list: List<Item?>) {
        (pagerRecyclerView.adapter as? PagerAdapter)?.submitList(list)
    }

    fun setOnSnapListener(listener: ((position: Int) -> Unit)?) {
        this.positionChangeListener = listener
    }

    fun setFocusListener(listener: ((v: View, hasFocus: Boolean) -> Unit)?) {
        this.focusListener = listener
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val savedState = SavedState(superState)
        savedState.selectedPosition = selectedPosition
        return savedState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        super.onRestoreInstanceState(state)
        (state as? SavedState)?.let {
            selectedPosition = it.selectedPosition
        }
    }

    class SavedState(superState: Parcelable? = AbsSavedState.EMPTY_STATE) : View.BaseSavedState(superState) {
        var selectedPosition: Int = 0
        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeValue(selectedPosition)
        }
    }

    open class Item(
            open var viewType: Int = TYPE_INPUT
    ) {
        companion object {
            const val TYPE_INPUT = 0
            const val TYPE_BUTTON = 1
            const val TYPE_CUSTOM = 2
        }
    }

    data class InputItem(
            override var viewType: Int = TYPE_INPUT,

            @StringRes
            val titleRes: Int,

            var value: String = "",

            val inputType: Int,

            val inputFilter: List<InputFilter> = listOf(),

            val onClickListener: OnClickListener? = null,

            val textWatcher: ((e: Editable?) -> Unit)? = null
    ) : Item()

    data class CustomItem(
            override var viewType: Int = TYPE_CUSTOM,

            val customView: View
    ) : Item()

    class Differ : DiffUtil.ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean =
                false

        override fun areContentsTheSame(oldItem: Item, newItem: Item) =
                false
    }

    inner class PagerAdapter : ListAdapter<Item, RecyclerView.ViewHolder>(Differ()) {

        override fun getItemViewType(position: Int) = getItem(position).viewType

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
            TYPE_BUTTON -> ButtonViewHolder(parent)
            TYPE_INPUT -> InputViewHolder(parent)
            else -> CustomViewHolder(parent)
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when (holder) {
                is InputViewHolder -> holder.bind()
                is ButtonViewHolder -> holder.bind()
                is CustomViewHolder -> holder.bind()
            }
        }

        inner class InputViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.card_input_item, parent, false)
        ) {

            fun bind() {
                val item = getItem(adapterPosition) as InputItem
                itemView.inputEditText.apply {
                    setText(item.value)
                    setRawInputType(item.inputType)
                    //inputType = item.inputType
                    filters = item.inputFilter.toTypedArray()
                    setOnFocusChangeListener { v, hasFocus ->
                        this@InputCardView.focusListener?.invoke(v, hasFocus)
                    }
                    imeOptions = if (adapterPosition != itemCount - 1) EditorInfo.IME_ACTION_NEXT
                    else EditorInfo.IME_ACTION_DONE
                    addTextChangedListener(InputTextWatcher())
                    setOnEditorActionListener { _, actionId, _ ->
                        if (actionId == EditorInfo.IME_ACTION_NEXT) {
                            selectedPosition += 1
                            true
                        } else false
                    }
                }
                itemView.inputTextLayout.hint = itemView.context.getString(item.titleRes)
            }

            inner class InputTextWatcher : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    (getItem(adapterPosition) as InputItem).apply {
                        value = s.toString()
                        textWatcher?.invoke(s)
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            }
        }

        inner class ButtonViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.card_input_button, parent, false)
        ) {

            fun bind() {
                val item = getItem(adapterPosition) as InputItem
                itemView.inputButton.apply {
                    setText(item.titleRes)
                    setOnClickListener(item.onClickListener)
                    setOnFocusChangeListener { v, hasFocus ->
                        this@InputCardView.focusListener?.invoke(v, hasFocus)
                    }
                }
            }
        }

        inner class CustomViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.card_input_custom, parent, false)
        ) {
            fun bind() {
                (itemView as FrameLayout).addView((getItem(adapterPosition) as CustomItem).customView)
            }
        }
    }
}