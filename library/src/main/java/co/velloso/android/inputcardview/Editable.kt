package co.velloso.android.inputcardview

import android.text.Editable

fun Editable.formatCreditCard(): Editable {
    if (this.toString().countOccurrences(" ") < 1 && this.length > 4) {
        this.insert(4, " ")
    }
    if (this.toString().countOccurrences(" ") < 2 && this.length > 9) {
        this.insert(9, " ")
    }
    if (this.toString().countOccurrences(" ") < 3 && this.length > 14) {
        this.insert(14, " ")
    }
    return this
}

fun Editable.formatValidThrough(): Editable {
    if (this.toString().countOccurrences("/") < 1 && this.length > 2) {
        this.insert(2, "/")
    }
    return this
}