package co.velloso.android.inputcardview

fun String.countOccurrences(occurrence: String): Int =
        this.length - this.replace(occurrence, "").length

fun String.isNumeric(): Boolean = try {
    java.lang.Double.parseDouble(this)
    true
} catch (nfe: NumberFormatException) {
    false
}