package co.velloso.android.inputcardview

import android.text.Editable
import android.text.TextWatcher

class BankCardHolderNameTextWatcher(private val callback: ((Editable?) -> Unit)) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        callback.invoke(s)
    }
}