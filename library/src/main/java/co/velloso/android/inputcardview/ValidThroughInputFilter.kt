package co.velloso.android.inputcardview

import android.text.InputFilter
import android.text.Spanned

class ValidThroughInputFilter : InputFilter {

    private val mMax = 5

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {

        var keep = mMax - (dest.length - (dend - dstart))
        if (keep <= 0) {
            return ""
        } else if (source.toString() == "/" && dstart != 2) {
            return ""
        } else if (!source.toString().isNumeric() && source.toString() != "/") {
            return ""
        } else if (keep >= end - start) {
            return null // keep original
        } else {
            keep += start
            if (Character.isHighSurrogate(source[keep - 1])) {
                --keep
                if (keep == start) {
                    return ""
                }
            }
            return source.subSequence(start, keep)
        }
    }
}
