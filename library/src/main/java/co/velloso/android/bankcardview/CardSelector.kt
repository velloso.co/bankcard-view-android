package co.velloso.android.bankcardview


interface CardSelector {
    fun getCardType(cardNumber: String?): BankCardView.CardType
}