package co.velloso.android.bankcardview

/**
 * Created by Harish on 01/01/16.
 */
class DefaultCardSelector : CardSelector {

    var cvvLength = CVV_LENGHT_DEFAULT

    override fun getCardType(cardNumber: String?): BankCardView.CardType {
        if (cardNumber != null && cardNumber.isNotEmpty()) {
            val cardType = CreditCardUtils.selectCardType(cardNumber)
            val selector = getCardType(cardType)

            if (selector != DEFAULT && cardNumber.length >= 3) {
                val drawables = intArrayOf(R.drawable.card_color_round_rect_brown, R.drawable.card_color_round_rect_green, R.drawable.card_color_round_rect_pink, R.drawable.card_color_round_rect_purple, R.drawable.card_color_round_rect_blue)
                var hash = cardNumber.substring(0, 3).hashCode()

                if (hash < 0) {
                    hash *= -1
                }

                val index = hash % drawables.size

                selector.resCardId = drawables[index]

                return selector
            }
        }

        return DEFAULT
    }

    private fun getCardType(cardType: CreditCardUtils.CardType): BankCardView.CardType {
        return when (cardType) {
            CreditCardUtils.CardType.AMEX_CARD -> AMEX
            CreditCardUtils.CardType.DISCOVER_CARD -> DISCOVER
            CreditCardUtils.CardType.MASTER_CARD -> MASTER
            CreditCardUtils.CardType.VISA_CARD -> VISA
            CreditCardUtils.CardType.UNKNOWN_CARD -> DEFAULT
        }
    }

    companion object {

        const val CVV_LENGHT_DEFAULT = 3
        const val CVV_LENGHT_AMEX = 4

        val VISA = BankCardView.CardType(R.drawable.card_color_round_rect_purple, android.R.color.transparent, R.drawable.ic_visa, CVV_LENGHT_DEFAULT)
        val MASTER = BankCardView.CardType(R.drawable.card_color_round_rect_blue, android.R.color.transparent, R.drawable.ic_mastercard, CVV_LENGHT_DEFAULT)
        val AMEX = BankCardView.CardType(R.drawable.card_color_round_rect_green, R.drawable.img_amex_center_face, R.drawable.ic_american_express, CVV_LENGHT_AMEX)
        val DISCOVER = BankCardView.CardType(R.drawable.card_color_round_rect_brown, android.R.color.transparent, R.drawable.ic_discover, CVV_LENGHT_DEFAULT)
        val DEFAULT = BankCardView.CardType(R.drawable.card_color_round_rect_default, android.R.color.transparent, android.R.color.transparent, CVV_LENGHT_DEFAULT)
    }
}
