package co.velloso.android.bankcardview

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Build
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import io.codetail.animation.ViewAnimationUtils
import kotlinx.android.synthetic.main.bank_card.view.*
import kotlinx.android.synthetic.main.card_back.view.*
import kotlinx.android.synthetic.main.card_front.view.*


class BankCardView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var currentDrawable: Int = R.drawable.card_color_round_rect_default

    private var rawCardNumber: String = ""

    var isFrontVisible = true

    var cardSelector: CardSelector = DefaultCardSelector()

    var cardNumber: String?
        get() = rawCardNumber
        set(value) {

            this.rawCardNumber = value ?: ""
            this.cardType = CreditCardUtils.selectCardType(this.rawCardNumber)
            val cardNumber = CreditCardUtils.formatCardNumber(this.rawCardNumber, CreditCardUtils.SPACE_SEPERATOR)

            frontCardNumber.text = cardNumber
            frontCardSecurityCode.visibility = if (cardType === CreditCardUtils.CardType.AMEX_CARD) View.VISIBLE else View.GONE

            if (this.cardType !== CreditCardUtils.CardType.UNKNOWN_CARD) {
                this.post { revealCardAnimation() }
            } else {
                paintCard()
            }
        }

    var cardHolderName: String = ""
        set(value) {
            field = if (value.length > cardNameLength) {
                value.substring(0, cardNameLength)
            } else value
            frontCardHolderName.text = cardHolderName
        }

    var securityCode: String = ""
        set(value) {
            field = value
            backCardSecurityCode.text = securityCode
            frontCardSecurityCode.text = securityCode
        }

    var expiry: String = ""
        set(value) {
            var dateYear = value

            dateYear = CreditCardUtils.handleExpiration(dateYear)

            field = value

            frontCardExpiry.text = dateYear
        }

    var cardType: CreditCardUtils.CardType? = null
        private set

    private var changeCardColor: Boolean = false

    private var showCardAnimation: Boolean = false

    var cardNameLength: Int = resources.getInteger(R.integer.card_name_len)

    init {

        inflate(context, R.layout.bank_card, this)

        val a = context.obtainStyledAttributes(attrs, R.styleable.BankCardView, 0, 0)

        cardHolderName = a.getString(R.styleable.BankCardView_card_holder_name) ?: ""
        expiry = a.getString(R.styleable.BankCardView_card_expiration) ?: ""
        cardNumber = a.getString(R.styleable.BankCardView_card_number)
        changeCardColor = a.getBoolean(R.styleable.BankCardView_change_card_color, true)
        showCardAnimation = a.getBoolean(R.styleable.BankCardView_show_card_animation, true)
        val showChipOnCard = a.getBoolean(R.styleable.BankCardView_show_chip_on_card, true)
        if (!showChipOnCard) {
            chipView!!.visibility = View.INVISIBLE
        }
        securityCode = a.getInt(R.styleable.BankCardView_card_security_code, 0).toString()
        val cardSide = a.getInt(R.styleable.BankCardView_card_side, CreditCardUtils.CARD_SIDE_FRONT)

        a.recycle()

        if (cardSide == CreditCardUtils.CARD_SIDE_BACK) showBackImmediate()
        else showFrontImmediate()

        paintCard()
    }

    fun flip() {
        if (isFrontVisible) showBack()
        else showFront()
    }

    fun showFront() {
        if (!isFrontVisible) {
            isFrontVisible = true
            flipInternal(ltr = true, isImmediate = false)
        }
    }

    fun showFrontImmediate() {
        isFrontVisible = true
        flipInternal(ltr = true, isImmediate = true)
    }

    fun showBack() {
        if (isFrontVisible) {
            isFrontVisible = false
            flipInternal(ltr = false, isImmediate = false)
        }
    }

    fun showBackImmediate() {
        isFrontVisible = false
        flipInternal(false, isImmediate = true)
    }

    private fun flipInternal(ltr: Boolean, isImmediate: Boolean) {

        if (isImmediate) {
            frontCardContainer.visibility = if (ltr) View.VISIBLE else View.GONE
            backCardContainer.visibility = if (ltr) View.GONE else View.VISIBLE

        } else {

            val duration = 600

            val flipAnimator = FlipAnimator(frontCardContainer, backCardContainer, frontCardContainer.width / 2, backCardContainer.height / 2)
            flipAnimator.interpolator = OvershootInterpolator(0.5f)
            flipAnimator.duration = duration.toLong()

            if (ltr) flipAnimator.reverse()

            flipAnimator.translateDirection = FlipAnimator.DIRECTION_Z
            flipAnimator.rotationDirection = FlipAnimator.DIRECTION_Y

            cardContainer.startAnimation(flipAnimator)
        }
    }

    private fun paintCard() {

        val card = getCardType()

        frontCardContainer.logoCenterImageView.setImageResource(card.resCenterImageId)
        frontCardContainer.topLogoImageView.setImageResource(card.resLogoId)

        backCardContainer.bottomLogoImageView.setImageResource(card.resLogoId)

        if (changeCardColor) {
            backCardContainer.setBackgroundResource(card.resCardId)
            frontCardContainer.setBackgroundResource(card.resCardId)
        }
    }

    private fun revealCardAnimation() {
        val card = getCardType()
        paintCard()
        if (showCardAnimation && changeCardColor) {
            animateChange(cardContainer, frontCardContainer, card.resCardId)
        }
    }

    fun getCardType(): CardType {
        return cardSelector.getCardType(rawCardNumber)
    }

    private fun animateChange(cardContainer: View, v: View, drawableId: Int) {
        showAnimation(cardContainer, v, drawableId)
    }

    private fun showAnimation(cardContainer: View, v: View, drawableId: Int) {

        v.setBackgroundResource(drawableId)

        if (currentDrawable == drawableId) {
            return
        }

        val duration = 1000
        val cx = v.left
        val cy = v.top

        val radius = Math.max(v.width, v.height) * 4

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            val animator = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0f, radius.toFloat())
            animator.setInterpolator(AccelerateDecelerateInterpolator())
            animator.setDuration(duration)

            Handler().postDelayed({ cardContainer.setBackgroundResource(drawableId) }, duration.toLong())

            v.visibility = View.VISIBLE
            animator.start()
            currentDrawable = drawableId

        } else {

            val anim = android.view.ViewAnimationUtils.createCircularReveal(v, cx, cy, 0f, radius.toFloat())
            v.visibility = View.VISIBLE
            anim.duration = duration.toLong()
            anim.start()
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)

                    cardContainer.setBackgroundResource(drawableId)
                }
            })

            currentDrawable = drawableId
        }
    }

    data class CardType(
            var resCardId: Int,
            val resCenterImageId: Int,
            val resLogoId: Int,
            val securityCodeLength: Int
    )
}
